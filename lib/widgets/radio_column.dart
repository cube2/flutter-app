import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class RadioColumn extends StatefulWidget {
  final Function(bool isPublic) notifyParent;
  final Map map;
  RadioColumn({Key key, @required this.notifyParent, @required this.map})
      : super(key: key);

  @override
  _RadioColumnState createState() => _RadioColumnState();
}

class _RadioColumnState extends State<RadioColumn> {
  int _chosenItem = 1;

  @override
  Widget build(BuildContext context) {
    List<Widget> l = [];
    widget.map.forEach((key, value) {
      InkWell row = InkWell(
        onTap: () {
          setState(() {
            _chosenItem = key;
            widget.notifyParent(value["isPublic"]);
          });
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 42,
              height: 42,
              decoration: BoxDecoration(
                  color: Color(0xff363842),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Padding(
                padding: EdgeInsets.all(8),
                child: SvgPicture.asset(
                  value["svg"],
                  width: 12,
                ),
              ),
            ),
            PaddingRight(value: 15),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextWithStyle(value["name"]),
                  Flexible(
                    child: Text(
                      value["description"],
                      textScaleFactor: 0.6,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      softWrap: false,
                      style: TextStyle(color: Colors.white.withOpacity(0.5)),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: Radio(
                  value: key,
                  groupValue: _chosenItem,
                  onChanged: (Object i) {
                    setState(() {
                      _chosenItem = i;
                      //_newGroup.isPublic = value["isPublic"];
                      widget.notifyParent(value["isPublic"]);
                    });
                  }),
            ),
          ],
        ),
      );
      l.add(row);
      if (key != widget.map.length - 1) {
        l.add(Divider(
          height: 20,
        ));
      }
    });

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: l,
    );
  }
}
