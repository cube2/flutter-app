import 'package:cubes_app/api/group_service.dart';
import 'package:cubes_app/api/pusher_service.dart';
import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/models/group.dart';
import 'package:cubes_app/widgets/group_row.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';
import 'package:pusher_websocket_flutter/pusher.dart';
import 'package:cubes_app/api/api_connection.dart' as api;

class UserGroupsList extends StatefulWidget {
  final PusherService pusherService;
  final List<Group> groups;
  UserGroupsList({Key key, @required this.pusherService, this.groups})
      : super(key: key);

  @override
  _UserGroupsListState createState() => _UserGroupsListState();
}

class _UserGroupsListState extends State<UserGroupsList> {
  String _lastReceivedContentId;

  @override
  Widget build(BuildContext context) {
    List<Group> _groups = widget.groups;

    return FutureBuilder<List<Group>>(
        future: getUserGroups(),
        builder: (BuildContext context, AsyncSnapshot futureSnapshot) {
          if (!futureSnapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          _groups = futureSnapshot.data;
          if (futureSnapshot.data.length == 0) {
            return Center(
              child: TextWithStyle(
                "Vous ne faites parti d'aucun groupe",
                color: Colors.grey[400],
              ),
            );
          } else {
            return StreamBuilder(
                stream: widget.pusherService.eventStream,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    Event pusher = snapshot.data;
                    if (pusher.event == "receiveMessage") {
                      Content newContent = contentFromJson(pusher.data);
                      //matching group
                      Group updatedGroup = _groups.firstWhere(
                          (gr) => 'group_${gr.id}' == pusher.channel);
                      if (_lastReceivedContentId != newContent.id)
                        updatedGroup.contents.add(newContent);
                      _lastReceivedContentId = newContent.id;
                      if (newContent.author.id != api.id) {
                        updatedGroup.unreadContents++;
                      }
                    }
                  }
                  _groups.sort((a, b) {
                    if (a.contents.isEmpty && b.contents.isEmpty) {
                      return 0;
                    } else if (a.contents.isEmpty && b.contents.isNotEmpty) {
                      return 1;
                    } else if (a.contents.isNotEmpty && b.contents.isEmpty) {
                      return -1;
                    }
                    return -a.contents?.last?.createdAt
                        ?.compareTo(b.contents?.last?.createdAt);
                  });
                  return ListView.builder(
                      itemCount: _groups.length,
                      itemBuilder: (context, i) {
                        Group group = _groups[i];
                        return GroupRow(
                            group: group, pusherService: widget.pusherService);
                      });
                });
          }
        });
  }
}
