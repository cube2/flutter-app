import 'package:cubes_app/api/api_connection.dart' as api;
import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';

class ChatMessage extends StatelessWidget {
  final Content content;

  ChatMessage({this.content});
  @override
  Widget build(BuildContext context) {
    final bool isTheConnectedUser = content.author.id == api.id;
    switch (content.type) {
      case 'image':
        if (isTheConnectedUser) {
          return SentImageMessage(content: content);
        } else {
          return ReceivedImageMessage(content: content);
        }
        break;
      default:
        if (isTheConnectedUser) {
          //connected user's messages
          return SentTextMessage(content: content);
        } else {
          //other messages
          return ReceivedTextMessage(content: content);
        }
        break;
    }
  }
}

class SentTextMessage extends StatelessWidget {
  final Content content;
  SentTextMessage({this.content});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
              child: Container(
                padding: EdgeInsets.all(15),
                width: MediaQuery.of(context).size.width * 0.75,
                decoration: BoxDecoration(
                    color: Color(0xff6ECDB7),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextWithStyle(this.content.value,
                          color: Color(0xff2C303E), factor: 1.2),
                    ]),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 12),
              child: TextWithStyle(
                this.content.getDateOrTime(),
                textAlign: TextAlign.end,
              ),
            )
          ],
        ),
      ],
    );
  }
}

class ReceivedTextMessage extends StatelessWidget {
  final Content content;
  ReceivedTextMessage({this.content});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
          child: Container(
            padding: EdgeInsets.all(15),
            width: MediaQuery.of(context).size.width * 0.75,
            decoration: BoxDecoration(
                color: Color(0xff2C303E),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CircleAvatar(
                            radius: 15,
                            backgroundImage: this.content.author.picture != null
                                ? NetworkImage(this.content.author.picture)
                                : null,
                          ),
                        PaddingRight(value: 8),
                        Text(this.content.author.getFullName()),
                      ],
                    ),
                    TextWithStyle(
                      this.content.getDateOrTime(),
                      color: Colors.grey[600],
                    ),
                  ],
                ),
                PaddingTop(value: 10),
                TextWithStyle(this.content.value,
                    color: Colors.white, factor: 1.2),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class SentImageMessage extends StatelessWidget {
  final Content content;
  SentImageMessage({this.content});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.75,
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                  color: Color(0xff6ECDB7),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Image.network(
                    this.content.value,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 12),
              child: TextWithStyle(
                this.content.getDateOrTime(),
                textAlign: TextAlign.end,
              ),
            )
          ],
        ),
      ],
    );
  }
}

class ReceivedImageMessage extends StatelessWidget {
  final Content content;
  ReceivedImageMessage({this.content});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
          child: Container(
            //padding: EdgeInsets.all(15),
            width: MediaQuery.of(context).size.width * 0.75,
            decoration: BoxDecoration(
                color: Color(0xff2C303E),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(15, 15, 15, 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CircleAvatar(
                            radius: 15,
                            backgroundImage: this.content.author.picture != null
                                ? NetworkImage(this.content.author.picture)
                                : null,
                          ),
                          PaddingRight(value: 8),
                          Text(this.content.author.getFullName()),
                        ],
                      ),
                      TextWithStyle(
                        this.content.getDateOrTime(),
                        color: Colors.grey[600],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(5),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: Image.network(
                      this.content.value,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
