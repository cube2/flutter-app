import 'package:cubes_app/api/pusher_service.dart';
import 'package:cubes_app/models/group.dart';
import 'package:cubes_app/views/groups/conversation_page.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:flutter/material.dart';

class PublicGroupModal extends StatelessWidget {
  final Group group;
  PublicGroupModal({@required this.group});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.fromLTRB(
            8, 8, 8, (MediaQuery.of(context).viewInsets.bottom) + 8.0),
        color: Colors.transparent,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
              color: Color(0xff22252F),
              borderRadius: BorderRadius.all(
                Radius.circular(30.0),
              )),
          child: Column(
            children: [
              CircleAvatar(
                radius: 42,
                backgroundImage:
                    group.picture == null ? null : NetworkImage(group.picture),
              ),
              PaddingTop(),
              Expanded(
                child: Text(group.description ?? "",
                    textScaleFactor: 0.9,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                        color: Colors.white.withOpacity(0.5),
                        fontWeight: FontWeight.w600)),
              ),
              PaddingTop(),
              Text("5 membres",
                  textScaleFactor: 0.9,
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.5),
                      fontWeight: FontWeight.w600)),
              PaddingTop(),
              ElevatedButton(
                onPressed: () {
                  //join
                  Navigator.pop(context);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ConversationPage(
                          group: group,
                          pusherService: new PusherService(),
                        ),
                      ));
                },
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0))),
                child: Text(
                  "Rejoindre",
                  textScaleFactor: 1.1,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w600),
                ),
              ),
              PaddingTop(value: 10),
            ],
          ),
        ),
      ),
    );
  }
}
