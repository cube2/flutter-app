import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ChooseAvatarCircle extends StatelessWidget {
  final File file;
  ChooseAvatarCircle({this.file});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CircleAvatar(
          radius: 50.0,
          backgroundColor: Colors.grey[700],
          child: CircleAvatar(
            backgroundColor: Color(0xff191A23),
            backgroundImage: (this.file == null) ? null : FileImage(this.file),
            radius: 49,
          ),
        ),
        Positioned(
          right: 0,
          bottom: 0,
          child: Material(
            child: Ink(
              height: 35,
              width: 35,
              decoration: const ShapeDecoration(
                color: Color(0xFF6ECDB7),
                shape: CircleBorder(),
              ),
              child: Center(
                child: SvgPicture.asset(
                  'assets/images/icn-plus.svg',
                  semanticsLabel: 'plus',
                  width: 24,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
