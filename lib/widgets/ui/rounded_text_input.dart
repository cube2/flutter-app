import 'package:flutter/material.dart';

class RoundedTextInput extends TextFormField {
  RoundedTextInput(
      {String label,
      void Function(String) onChanged,
      void Function() onTap,
      TextEditingController controller,
      TextInputType keyboardType: TextInputType.text,
      int minLines = 1,
      int maxLines = 1,
      bool isPassword: false})
      : super(
          onChanged: onChanged,
          onTap: onTap,
          controller: controller,
          decoration: new InputDecoration(
            labelText: label,
            border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(25.0),
              borderSide: new BorderSide(),
            ),
          ),
          keyboardType: keyboardType,
          minLines: minLines,
          maxLines: maxLines,
          obscureText: isPassword,
          enableSuggestions: !isPassword,
          autocorrect: !isPassword,
          style: new TextStyle(
            fontFamily: "Poppins",
          ),
        );
}
