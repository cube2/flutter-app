import 'package:cubes_app/widgets/event_modal.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ChooseMediaModal extends StatelessWidget {
  final Function(String choice) notifyParent;
  final String groupId;
  final bool showEventOption;
  ChooseMediaModal(
      {Key key,
      @required this.notifyParent,
      this.groupId,
      @required this.showEventOption})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(
          13, 8, 13, (MediaQuery.of(context).viewInsets.bottom) + 15.0),
      color: Colors.transparent,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
            decoration: BoxDecoration(
                color: Color(0xff22252F),
                borderRadius: new BorderRadius.all(
                  const Radius.circular(30.0),
                )),
            child: Column(
              children: showEventOption
                  ? [
                      ListTile(
                        leading: Icon((Icons.photo_outlined), size: 26),
                        title: Text("Photo"),
                        onTap: () {
                          Navigator.pop(context);
                          notifyParent("photo");
                        },
                      ),
                      Divider(),
                      ListTile(
                        leading: SvgPicture.asset(
                          'assets/images/icn-calendar.svg',
                          semanticsLabel: 'calendar',
                          //width: 24,
                        ),
                        title: Text("Créer un évènement"),
                        onTap: () {
                          Navigator.pop(context);
                          showEventModal(context);
                        },
                      ),
                    ]
                  : [
                      ListTile(
                        leading: Icon((Icons.photo_outlined), size: 26),
                        title: Text("Photo"),
                        onTap: () {
                          Navigator.pop(context);
                          notifyParent("photo");
                        },
                      ),
                    ],
            ),
          ),
          PaddingTop(value: 10),
          Container(
            padding: EdgeInsets.fromLTRB(30, 5, 30, 5),
            decoration: BoxDecoration(
                color: Color(0xff22252F),
                borderRadius: new BorderRadius.all(
                  const Radius.circular(30.0),
                )),
            child: ListTile(
              title: TextWithStyle(
                "Annuler",
                factor: 1.0,
                textStyle: TextStyle(color: Color(0xFFE64A30)),
              ),
              onTap: () => Navigator.pop(context),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> showEventModal(BuildContext context) {
    return showMaterialModalBottomSheet(
        context: context,
        expand: false,
        barrierColor: Colors.black.withOpacity(0.7),
        builder: (context) {
          return EventModal(groupId: groupId);
        });
  }
}
