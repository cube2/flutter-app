import 'package:flutter/material.dart';

class NotificationCircle extends StatelessWidget {
  final int value;
  NotificationCircle(this.value);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 10,
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: LinearGradient(
            colors: [
              Color(0xff8a2387),
              Color(0xffe94057),
              Color(0xfff27121),
            ],
          ),
        ),
        child: Center(
          child: Text(
            this.value.toString(),
            textScaleFactor: 0.9,
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
        ),
      ),
    );
  }
}
