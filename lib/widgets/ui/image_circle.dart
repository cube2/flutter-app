import 'dart:io';

import 'package:flutter/material.dart';

class ImageCircle extends StatelessWidget {
  final File file;
  ImageCircle({this.file});
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 50.0,
      backgroundColor: Colors.grey[700],
      child: CircleAvatar(
        backgroundImage: FileImage(this.file),
        radius: 49,
      ),
    );
  }
}
