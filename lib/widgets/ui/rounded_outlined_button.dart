import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';

class RoundedOutlinedButton extends OutlinedButton {
  RoundedOutlinedButton(String data,
      {void Function() onPressed, Color color: Colors.white})
      : super(
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
              padding: EdgeInsets.fromLTRB(25, 12, 25, 12),
              side: BorderSide(width: 1.0, color: color),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
              )),
          child: TextWithStyle(
            data,
            color: color,
          ),
        );
}
