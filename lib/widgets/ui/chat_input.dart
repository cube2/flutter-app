import 'dart:io';

import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/models/user.dart';
import 'package:cubes_app/widgets/ui/choose_media_modal.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/rounded_text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:cubes_app/api/api_connection.dart' as api;
import 'package:cubes_app/api/file_service.dart' as cloud;

class ChatInput extends StatefulWidget {
  final Function(Content content) notifyParent;
  final String groupId;
  final bool isGroup;
  ChatInput({Key key, @required this.notifyParent, this.groupId, @required this.isGroup})
      : super(key: key);

  @override
  _ChatInputState createState() => _ChatInputState();
}

class _ChatInputState extends State<ChatInput> {
  TextEditingController textController = TextEditingController();
  //bool showSendIcon = false;
  File _imageInput;

  @override
  Widget build(BuildContext context) {
    return _imageInput == null ? textInput() : imageInput();
  }

  Widget textInput() {
    //return Flexible(
    // return ConstrainedBox(
    //   //child: Container(color: Colors.red),
    //   //
    //   constraints: BoxConstraints(
    //     minWidth: 50,
    //     maxWidth: 200,
    //   ),
    //),
    return Container(
      height: 50.0,
      color: Color(0xff2C303E),
      child: Row(
        //crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          PaddingRight(value: 8),
          Material(
            child: Ink(
              height: 35,
              width: 35,
              decoration: const ShapeDecoration(
                color: Color(0xFF6ECDB7),
                shape: CircleBorder(),
              ),
              child: InkWell(
                child: IconButton(
                  icon: SvgPicture.asset(
                    'assets/images/icn-plus.svg',
                    semanticsLabel: 'plus',
                    width: 24,
                  ),
                  color: Colors.white,
                  onPressed: () {
                    showChooseMediaModal();
                  },
                ),
              ),
            ),
          ),
          //       ConstrainedBox(constraints: BoxConstraints(
          //   minHeight: 50,
          //   maxHeight: 200,
          // ),
          //child:
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 20.0),
              // child: TextFormField(
              //   keyboardType: TextInputType.multiline,
              //   minLines: 1,
              //   maxLines: 5,
              child: RoundedTextInput(
                label: "Aa",
                // onChanged: (s) {
                //   setState(() {
                //     showSendIcon = (textController.text != "") ? true : false;
                //   });
                // },
                controller: textController,
              ),
            ),
          ),

          Material(
            color: Color(0xff2C303E),
            child: InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Icon(
                  //showSendIcon ?
                  Icons.send,
                  //: Icons.mic,
                  color: Colors.white,
                ),
              ),
              onTap: sendText,
            ),
          ),
        ],
      ),
    );
  }

  Widget imageInput() {
    return Container(
      color: Color(0xff2C303E),
      height: 200,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          PaddingRight(value: 8),
          Material(
            child: Ink(
              height: 35,
              width: 35,
              decoration: const ShapeDecoration(
                color: Color(0xFF6ECDB7),
                shape: CircleBorder(),
              ),
              child: InkWell(
                child: IconButton(
                  icon: SvgPicture.asset(
                    'assets/images/icn-plus.svg',
                    semanticsLabel: 'plus',
                    width: 24,
                  ),
                  color: Colors.white,
                  onPressed: () {
                    resetPicture();
                    showChooseMediaModal();
                  },
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 10, 0, 10),
              child: Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Color(0xff22252F),
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  border: Border.all(color: Colors.white.withOpacity(0.2)),
                ),
                child: FractionallySizedBox(
                  widthFactor: 0.6,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: Image.file(
                          _imageInput,
                          fit: BoxFit.cover,
                        ),
                      ),
                      Positioned(
                          right: 0,
                          width: 48,
                          child: RawMaterialButton(
                            onPressed: resetPicture,
                            fillColor: Colors.transparent.withOpacity(0.5),
                            child: Icon(
                              Icons.close,
                            ),
                            shape: CircleBorder(),
                          ))
                    ],
                  ),
                ),
              ),
            ),
          ),
          Material(
            color: Color(0xff2C303E),
            child: InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0),
                child: Icon(
                  Icons.send,
                  color: Colors.white,
                ),
              ),
              onTap: sendPicture,
            ),
          ),
        ],
      ),
    );
  }

  void resetPicture() {
    setState(() {
      _imageInput = null;
    });
  }

  Future<void> showChooseMediaModal() {
    return showMaterialModalBottomSheet(
        context: context,
        expand: false,
        barrierColor: Colors.black.withOpacity(0.3),
        builder: (context) {
          return ChooseMediaModal(
              notifyParent: choiceCallback, groupId: widget.groupId, showEventOption: widget.isGroup);
        });
  }

  //function for the choose_media_modal (child) to callback
  Future<void> choiceCallback(String choice) async {
    if (choice == "photo") {
      File chosenImage = await selectPicture();
      setState(() {
        _imageInput = chosenImage;
      });
    }
  }

  Future<File> selectPicture() async {
    final _picker = ImagePicker();
    final pickedFile = await _picker.getImage(
      source: ImageSource.gallery,
      maxWidth: 500,
      imageQuality: 60,
    );
    return (pickedFile != null) ? File(pickedFile.path) : null;
  }

  void sendText() {
    Content newContent = Content(
        type: 'text',
        value: textController.text,
        createdAt: DateTime.now().add(Duration(hours: 2)),
        author: User(id: api.id));
    textController.clear();
    //showSendIcon = false;
    widget.notifyParent(newContent);
  }

  Future<void> sendPicture() async {
    //upload file to cloudinary
    String uploadedImage = await cloud.uploadImage(_imageInput);
    Content newContent = Content(
        type: "image",
        value: uploadedImage,
        createdAt: DateTime.now().add(Duration(hours: 2)),
        author: User(id: api.id));
    resetPicture();
    widget.notifyParent(newContent);
  }
}
