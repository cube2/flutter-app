import 'package:flutter/material.dart';

class PaddingTop extends Padding {
  PaddingTop({double value: 20.0}):
  super(padding: EdgeInsets.only(top: value));
}
