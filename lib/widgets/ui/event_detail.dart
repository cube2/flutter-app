import 'package:cubes_app/api/event_service.dart';
import 'package:cubes_app/models/group_event.dart';
import 'package:cubes_app/views/groups/event_page.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EventDetail extends StatelessWidget {
  final GroupEvent event;
  EventDetail({@required this.event});

  @override
  Widget build(BuildContext context) {
    Intl.defaultLocale = "fr_FR";
    return InkWell(
      onTap: () async {
        GroupEvent fullEvent = await getEvent(event.id);
        String currentRoute = ModalRoute.of(context).settings.name;
        if (currentRoute != "/event") {
          Navigator.of(context).pop();
        }
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EventPage(event: fullEvent),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.2),
            border: Border.all(color: Colors.white.withOpacity(0.3)),
            borderRadius: BorderRadius.all(Radius.circular(30.0))),
        child: Column(
          children: [
            //1st row
            Row(
              children: [
                CircleAvatar(
                  radius: 24,
                  backgroundImage: event.picture != null
                      ? NetworkImage(this.event.picture)
                      : null,
                ),
                PaddingRight(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(event.name),
                    Text(event.description ?? "",
                        textScaleFactor: 0.7,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.white.withOpacity(0.5),
                            fontWeight: FontWeight.w600)),

                    // Text(event.description ?? "",
                    //     textScaleFactor: 0.7,
                    //     style: TextStyle(
                    //         color: Colors.white
                    //             .withOpacity(0.5)))
                  ],
                )
              ],
            ),
            PaddingTop(value: 10),
            //2nd row
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    TextWithStyle(
                      "Date :",
                      factor: 1.0,
                      color: Colors.white.withOpacity(0.5),
                    ),
                    Text(
                      DateFormat('d MMM').format(event.date),
                    )
                  ],
                ),
                PaddingRight(),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      TextWithStyle(
                        "Lieu :",
                        factor: 1.0,
                        color: Colors.white.withOpacity(0.5),
                      ),
                      Text(event.location ?? "")
                    ],
                  ),
                ),
                PaddingRight(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextWithStyle(
                      "Par :",
                      factor: 1.0,
                      color: Colors.white.withOpacity(0.5),
                    ),
                    CircleAvatar(
                      radius: 12,
                      backgroundImage: event.author.picture != null
                          ? NetworkImage(this.event.author.picture)
                          : null,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
