import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class IconBubbleButton extends InkWell {
  IconBubbleButton({
    Function() onClick,
  }) : super(
          onTap: onClick,
          child: Container(
            alignment: Alignment.center,
            width: 60.0,
            height: 60.0,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Color(0xFF6ECDB7),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                bottomLeft: Radius.circular(30.0),
                topRight: Radius.circular(25.0),
                bottomRight: Radius.zero,
              ),
            ),
            child: SvgPicture.asset('assets/images/icn-plus.svg',
                semanticsLabel: 'plus', width: 36),
          ),
        );
}
