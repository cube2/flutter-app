import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BottomNav extends BottomNavigationBar {
  BottomNav(BuildContext context)
      : super(
          backgroundColor: Color(0xff22252F),
          selectedItemColor: Color(0xb3ffffff),
          items: [
            BottomNavigationBarItem(
                icon: SvgPicture.asset('assets/images/icn-home.svg',
                    semanticsLabel: 'home', width: 24),
                label: "Accueil"),
            BottomNavigationBarItem(
                icon: SvgPicture.asset('assets/images/icn-msg.svg',
                    semanticsLabel: 'chat bubble', width: 24),
                label: "Conversations"),
            BottomNavigationBarItem(
                icon: SvgPicture.asset('assets/images/icn-calendar.svg',
                    semanticsLabel: 'calendar', width: 24),
                label: "Evènements"),
          ],
          onTap: (int index) {
            String currentRoute = ModalRoute.of(context).settings.name;
            switch (index) {
              case 0:
                if (currentRoute != "/home") {
                  Navigator.pushNamedAndRemoveUntil(
                      context, "/home", (route) => false);
                }
                break;
              case 1:
                if (currentRoute != "/group") {
                  Navigator.pushNamedAndRemoveUntil(
                      context, "/group", (route) => false);
                }
                break;
              case 2:
                if (currentRoute != "/event") {
                  Navigator.pushNamedAndRemoveUntil(
                      context, "/event", (route) => false);
                }
                break;
            }
          },
        );
}
