import 'package:flutter/material.dart';

class PaddingRight extends Padding {
  PaddingRight({double value: 20.0}):
  super(padding: EdgeInsets.only(right: value));
}
