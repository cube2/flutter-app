import 'package:flutter/material.dart';

class ImageBubbleButton extends InkWell {
  ImageBubbleButton({Function() onClick, String image})
      : super(
          onTap: onClick,
          child: Container(
            alignment: Alignment.center,
            width: 60.0,
            height: 60.0,
            decoration: BoxDecoration(
              image: DecorationImage(fit: BoxFit.fill, image: NetworkImage(image)),
              shape: BoxShape.rectangle,
              color: Color(0xFF6ECDB7),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.0),
                bottomLeft: Radius.circular(30.0),
                topRight: Radius.circular(25.0),   
                bottomRight: Radius.zero,
              ),
            ),
          ),
        );
}
