import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';

class CustomElevatedButton extends SizedBox {
  CustomElevatedButton(String data,
      {double width,
      void Function() onPressed,
      Color primary: Colors.white,
      Color textColor: Colors.black})
      : super(
            width: width,
            child: ElevatedButton(
              onPressed: onPressed,
              child: TextWithStyle(data, color: textColor),
              style: ElevatedButton.styleFrom(
                  primary: primary,
                  padding: EdgeInsets.all(15.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(15.0))),
            ));
}
