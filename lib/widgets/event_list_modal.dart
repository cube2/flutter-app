import 'package:cubes_app/models/group_event.dart';
import 'package:cubes_app/widgets/ui/event_detail.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';

class EventListModal extends StatelessWidget {
  final List<GroupEvent> events;
  EventListModal({@required this.events});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.fromLTRB(
            8, 8, 8, (MediaQuery.of(context).viewInsets.bottom) + 8.0),
        color: Colors.transparent,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.5,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
              color: Color(0xff22252F),
              borderRadius: BorderRadius.all(
                Radius.circular(30.0),
              )),
          child: Column(
            children: [
              Text(
                "Évènements à venir",
                textScaleFactor: 1.4,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
              ),
              Expanded(
                  child: events.isEmpty
                      ? Center(
                          child: TextWithStyle(
                            "Aucun évènement disponible pour le moment",
                            color: Colors.grey[400],
                          ),
                        )
                      : ListView.builder(
                          itemCount: events.length,
                          itemBuilder: (context, i) {
                            GroupEvent event = events[i];
                            return EventDetail(event: event);
                          })),
            ],
          ),
        ),
      ),
    );
  }
}
