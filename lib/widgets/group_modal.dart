import 'dart:io';

import 'package:cubes_app/api/group_service.dart';
import 'package:cubes_app/api/pusher_service.dart';
import 'package:cubes_app/api/user_service.dart';
import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/models/group.dart';
import 'package:cubes_app/models/group_event.dart';
import 'package:cubes_app/models/user.dart';
import 'package:cubes_app/views/groups/conversation_page.dart';
import 'package:cubes_app/widgets/radio_column.dart';
import 'package:cubes_app/widgets/ui/choose_avatar_circle.dart';
import 'package:cubes_app/widgets/ui/image_circle.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/rounded_outlined_button.dart';
import 'package:cubes_app/widgets/ui/rounded_text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cubes_app/api/file_service.dart' as cloud;

class GroupModal extends StatefulWidget {
  final PusherService pusherService;
  GroupModal({Key key, this.pusherService}) : super(key: key);

  @override
  _GroupModalState createState() => _GroupModalState();
}

class _GroupModalState extends State<GroupModal> {
  File _storedImage;
  Map _mapType = {
    0: {
      "name": "Public",
      "description": "Tout le monde peut voir et rejoindre ce groupe",
      "svg": "assets/images/icn-globe.svg",
      "isPublic": true
    },
    1: {
      "name": "Privé",
      "description": "Seules les personnes invitées peuvent voir ce groupe",
      "svg": "assets/images/icn-lock.svg",
      "isPublic": false
    },
  };
  Group _newGroup = Group(isPublic: false, contents: <Content>[]);
  List<User> _activeUsers;
  List<String> _newGroupMembersId = <String>[];
  bool _addMembers = false;

  @override
  void initState() {
    super.initState();
    fetchUsers();
  }

  Future<void> fetchUsers() async {
    _activeUsers = await getActiveUsersButMe();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.fromLTRB(
            8, 8, 8, (MediaQuery.of(context).viewInsets.bottom) + 8.0),
        color: Colors.transparent,
        child: Container(
          padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
          decoration: BoxDecoration(
              color: Color(0xff22252F),
              borderRadius: new BorderRadius.all(
                const Radius.circular(30.0),
              )),
          child: Column(
            children: _addMembers
                ? [
                    //AddMembers
                    ImageCircle(file: _storedImage),
                    PaddingTop(),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.5,
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: _activeUsers.length,
                          itemBuilder: (context, i) {
                            User user = _activeUsers[i];
                            return ListTile(
                                title: Text(
                                  user.getFullName(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600),
                                ),
                                leading: CircleAvatar(
                                  radius: 22,
                                  backgroundImage: user.picture != null
                                      ? NetworkImage(user.picture)
                                      : null,
                                ),
                                trailing: _newGroupMembersId.contains(user.id)
                                    ? SvgPicture.asset(
                                        'assets/images/checkbox_on.svg',
                                        semanticsLabel: 'plus',
                                        width: 24)
                                    : SvgPicture.asset(
                                        'assets/images/checkbox_off.svg',
                                        semanticsLabel: 'plus',
                                        width: 24),
                                onTap: () {
                                  setState(() {
                                    if (_newGroupMembersId.contains(user.id)) {
                                      _newGroupMembersId.remove(user.id);
                                    } else {
                                      _newGroupMembersId.add(user.id);
                                    }
                                  });
                                });
                          }),
                    ),
                    PaddingTop(),
                    RoundedOutlinedButton(
                      "Terminer",
                      onPressed: () {
                        if (_newGroupMembersId.isNotEmpty) {
                          finalizeGroup();
                        }
                      },
                    ),
                  ]
                : [
                    //Initial Modal Page
                    InkWell(
                      onTap: () async {
                        File chosenImage = await selectPicture();
                        setState(() {
                          _storedImage = chosenImage;
                        });
                      },
                      child: ChooseAvatarCircle(file: _storedImage),
                    ),
                    PaddingTop(),
                    RoundedTextInput(
                      onChanged: (s) {
                        _newGroup.name = s;
                      },
                      label: "Nom du groupe",
                    ),
                    PaddingTop(),
                    RoundedTextInput(
                      onChanged: (s) {
                        _newGroup.description = s;
                      },
                      label: "Description",
                      keyboardType: TextInputType.multiline,
                      minLines: 6,
                      maxLines: 6,
                    ),
                    PaddingTop(),
                    RadioColumn(
                      notifyParent: updateGroupType,
                      map: _mapType,
                    ),
                    RoundedOutlinedButton(
                      "Suivant",
                      onPressed: () {
                        if (_newGroup.name != null && _storedImage != null) {
                          createGroup();
                        }
                      },
                    ),
                  ],
          ),
        ),
      ),
    );
  }

  Future<File> selectPicture() async {
    final _picker = ImagePicker();
    final pickedFile = await _picker.getImage(
      source: ImageSource.gallery,
      maxWidth: 500,
      imageQuality: 60,
    );
    return (pickedFile != null) ? File(pickedFile.path) : null;
  }

  //function for the radio_column (child) to callback
  void updateGroupType(bool isPublic) {
    setState(() {
      _newGroup.isPublic = isPublic;
    });
  }

  Future<void> createGroup() async {
    if (_storedImage != null) {
      _newGroup.picture = await cloud.uploadImage(_storedImage);
    }
    //TODO call group_service  to add group then reset group for new creation then go to conversation, of this group
    String groupId = await addGroup(_newGroup);
    {
      if (groupId != null) {
        setState(() {
          _newGroup.id = groupId;
          _addMembers = true;
        });
      }
    }
    //close modal and reset data
  }

  Future<void> finalizeGroup() async {
    Group createdGroup = _newGroup;
    createdGroup.userIds = _newGroupMembersId;
    createdGroup.events = <GroupEvent>[];
    await addMembers(createdGroup);
    Navigator.pop(context);
    setState(() {
      _newGroup = new Group(isPublic: false, contents: <Content>[]);
      _storedImage = null;
      _addMembers = false;
    });
    await widget.pusherService.subscribePusher('group_${_newGroup.id}');
    widget.pusherService.bindEvent('group_${_newGroup.id}', 'receiveMessage');
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ConversationPage(
            group: createdGroup, pusherService: widget.pusherService),
      ),
    );
  }
}
