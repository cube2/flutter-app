import 'dart:io';

import 'package:cubes_app/api/event_service.dart';
import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/models/group_event.dart';
import 'package:cubes_app/views/groups/event_page.dart';
import 'package:cubes_app/widgets/ui/choose_avatar_circle.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/rounded_outlined_button.dart';
import 'package:cubes_app/widgets/ui/rounded_text_input.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:cubes_app/api/file_service.dart' as cloud;
import 'package:intl/intl.dart';

class EventModal extends StatefulWidget {
  final String groupId;
  EventModal({Key key, @required this.groupId}) : super(key: key);

  @override
  _EventModalState createState() => _EventModalState();
}

class _EventModalState extends State<EventModal> {
  File _storedImage;
  GroupEvent _newEvent =
      GroupEvent(date: DateTime.now(), contents: <Content>[]);

  @override
  void initState() {
    _newEvent.groupId = widget.groupId;
    super.initState();
    Intl.defaultLocale = "fr_FR"; //sets global
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.fromLTRB(
            8, 8, 8, (MediaQuery.of(context).viewInsets.bottom) + 8.0),
        color: Colors.transparent,
        child: Container(
          padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
          decoration: BoxDecoration(
              color: Color(0xff22252F),
              borderRadius: new BorderRadius.all(
                const Radius.circular(30.0),
              )),
          child: Column(
            children: [
              InkWell(
                onTap: () async {
                  File chosenImage = await selectPicture();
                  setState(() {
                    _storedImage = chosenImage;
                  });
                },
                child: ChooseAvatarCircle(file: _storedImage),
              ),
              PaddingTop(),
              RoundedTextInput(
                onChanged: (s) {
                  _newEvent.name = s;
                },
                label: "Nom de l'évènement",
              ),
              PaddingTop(),
              RoundedTextInput(
                onChanged: (s) {
                  _newEvent.description = s;
                },
                label: "Description",
                keyboardType: TextInputType.multiline,
                minLines: 4,
                maxLines: 4,
              ),
              PaddingTop(),
              Container(
                alignment: Alignment.centerLeft,
                child:
                    TextWithStyle("Date", color: Colors.white.withOpacity(0.5)),
              ),
              OutlinedButton(
                child: IntrinsicHeight(
                  child: Row(
                    children: [
                      Flexible(
                        fit: FlexFit.tight,
                        child: TextWithStyle(
                          DateFormat('EEEE d MMMM').format(_newEvent.date),
                          color: Colors.white,
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Row(children: [
                        VerticalDivider(
                          color: Colors.white,
                        ),
                        SvgPicture.asset('assets/images/icn-calendar.svg',
                            semanticsLabel: 'calendar',
                            width: 18,
                            color: Colors.white.withOpacity(0.3)),
                      ]),
                    ],
                  ),
                ),
                onPressed: _selectDate,
                style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.fromLTRB(25, 12, 25, 12),
                    side: BorderSide(
                        width: 0.5, color: Colors.white.withOpacity(0.5)),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    primary: Color(0xff191A23)),
              ),
              PaddingTop(),
              RoundedTextInput(
                onChanged: (s) {
                  _newEvent.location = s;
                },
                label: "Lieu",
              ),
              PaddingTop(),
              RoundedOutlinedButton(
                "Créer",
                onPressed: () {
                  if (_newEvent.name != null && _storedImage != null) {
                    createEvent();
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<File> selectPicture() async {
    final _picker = ImagePicker();
    final pickedFile = await _picker.getImage(
      source: ImageSource.gallery,
      maxWidth: 500,
      imageQuality: 60,
    );
    return (pickedFile != null) ? File(pickedFile.path) : null;
  }

  Future<void> _selectDate() async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _newEvent.date,
        firstDate: DateTime.now(),
        lastDate: DateTime(2050));
    if (picked != null && picked != _newEvent.date)
      setState(() {
        _newEvent.date = picked;
      });
  }

  Future<void> createEvent() async {
    if (_storedImage != null) {
      _newEvent.picture = await cloud.uploadImage(_storedImage);
    }
    //TODO call event_service  to add event then reset event for new creation then go to conversation, of this event
    GroupEvent createdEvent = _newEvent;
    String eventId = await addEvent(_newEvent);
    if (eventId != null) {
      createdEvent.id = eventId;
      //close modal and reset data
      Navigator.pop(context);
      setState(() {
        _newEvent = new GroupEvent(date: DateTime.now(), contents: <Content>[]);
        _storedImage = null;
      });
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EventPage(event: createdEvent),
        ),
      );
    }
  }
}
