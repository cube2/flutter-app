import 'package:cubes_app/api/group_service.dart';
import 'package:cubes_app/api/pusher_service.dart';
import 'package:cubes_app/models/group.dart';
import 'package:cubes_app/views/groups/conversation_page.dart';
import 'package:cubes_app/widgets/ui/notification_circle.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class GroupRow extends StatelessWidget {
  final Group group;
  final PusherService pusherService;
  GroupRow({this.group, this.pusherService});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 18),
      child: ListTile(
        onTap: () async {
          //api call getGroup
          Group fullGroup = await getGroup(group.id);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ConversationPage(
                group: fullGroup,
                pusherService: pusherService,
              ),
            ),
          );
        },
        leading: CircleAvatar(
          radius: 30,
          backgroundImage: NetworkImage(group.picture),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
                children: group.isPublic
                    ? [
                        Text(group.name,
                            textScaleFactor: 1.1,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600)),
                      ]
                    : [
                        Text(group.name,
                            textScaleFactor: 1.1,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600)),
                        PaddingRight(),
                        SvgPicture.asset(
                          'assets/images/icn-lock.svg',
                          semanticsLabel: 'lock',
                          width: 24,
                          color: Colors.white.withOpacity(0.5),
                        ),
                      ]),
            PaddingTop(
              value: 5,
            ),
            Row(
              children: group.contents.length == 0
                  ? [PaddingTop(value: 20)]
                  : [
                      CircleAvatar(
                        radius: 10,
                        backgroundImage: group.contents.last.author.picture !=
                                null
                            ? NetworkImage(group.contents.last.author.picture)
                            : null,
                      ),
                      PaddingRight(value: 5),
                      Flexible(
                        child: Text(
                            group.contents.last.type == "image"
                                ? "a envoyé une image"
                                : group.contents.last.value,
                            overflow: TextOverflow.ellipsis,
                            textScaleFactor: 0.7,
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.5),
                            )),
                      ),
                    ],
            ),
          ],
        ),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: group.unreadContents > 0
              ? [
                  NotificationCircle(group.unreadContents),
                  Text(
                      group.contents.length == 0
                          ? ""
                          : group.contents.last.getDateOrTime(),
                      textScaleFactor: 0.8,
                      style: TextStyle(
                          color: Colors.white.withOpacity(0.5),
                          fontWeight: FontWeight.w600)),
                ]
              : [
                  PaddingTop(),
                  Text(
                      group.contents.length == 0
                          ? ""
                          : group.contents.last.getDateOrTime(),
                      textScaleFactor: 0.8,
                      style: TextStyle(
                          color: Colors.white.withOpacity(0.5),
                          fontWeight: FontWeight.w600)),
                ],
        ),
      ),
    );
  }
}
