import 'package:cubes_app/views/auth/index_page.dart';
import 'package:cubes_app/views/auth/login_page.dart';
import 'package:cubes_app/views/auth/register_2_page.dart';
import 'package:cubes_app/views/auth/register_3_page.dart';
import 'package:cubes_app/views/auth/register_page.dart';
import 'package:cubes_app/views/event_list_page.dart';
import 'package:cubes_app/views/groups/group_page.dart';
import 'package:cubes_app/views/home_page.dart';
import 'package:flutter/material.dart';

final Map routes = <String, WidgetBuilder>{
  "/home": (BuildContext context) => HomePage(),
  "/index": (BuildContext context) => IndexPage(title: "Agora App"),
  "/login": (BuildContext context) => LoginPage(),
  "/register": (BuildContext context) => RegisterPage(),
  "/register2": (BuildContext context) => Register2Page(),
  "/register3": (BuildContext context) => Register3Page(),
  "/group": (BuildContext context) => GroupPage(),
  // "/conversation": (BuildContext context) => ConversationPage(group: null,),
  "/event": (BuildContext context) => EventListPage(),
};
