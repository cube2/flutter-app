import 'dart:io';
import 'package:dio/dio.dart';

final String _path = 'https://api.cloudinary.com/v1_1/theo-cesi/image/upload';
final String _uploadReset = 'h19ajqlm';
final String _cloudName = 'theo-cesi';
final String _folder = 'picture_group';

Future uploadImage(File file) async {
  Dio dio = Dio();
  FormData formData = new FormData.fromMap({
    "file": await MultipartFile.fromFile(file.path),
    "upload_preset": _uploadReset,
    "cloud_name": _cloudName,
    "folder": _folder
  });
  try {
    Response response = await dio.post(_path, data: formData);
    return response.data['secure_url'];
  } catch (e) {
    print(e.message);
    print(e.request);
  }
}
