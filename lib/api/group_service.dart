import 'dart:convert';

import 'package:cubes_app/models/group.dart';
import 'package:http/http.dart' as http;
import 'package:cubes_app/api/api_connection.dart' as api;

Future<List<Group>> getPublicGroups() async {
  final response = await http.get(Uri.http(api.url, '/all_public_groups'),
      headers: api.headers);
  Iterable parsedListJson = json.decode(response.body);
  return List<Group>.from(parsedListJson.map((model) => Group.fromJson(model)));
}

Future<List<Group>> getUserGroups() async {
  final response =
      await http.get(Uri.http(api.url, '/user_groups'), headers: api.headers);
  Iterable parsedListJson = json.decode(response.body);
  return List<Group>.from(parsedListJson.map((model) => Group.fromJson(model)));
}

Future<Group> getGroup(String id) async {
  final response = await http.get(Uri.http(api.url, '/get_group/' + id),
      headers: api.headers);
  return groupFromJson(response.body);
}

Future<String> addGroup(Group group) async {
  final response = await http.post(Uri.http(api.url, '/created_group'),
      headers: api.headers, body: groupToJson(group));
  Map<String, dynamic> jsonBody = json.decode(response.body);
  return jsonBody["id_group"];
}

Future<http.Response> addMembers(Group group) async {
  final response = await http.post(Uri.http(api.url, '/add_members'),
      headers: api.headers, body: groupToJson(group));
  return response;
}
