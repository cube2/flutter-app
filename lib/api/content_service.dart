import 'package:cubes_app/models/content.dart';
import 'package:http/http.dart' as http;
import 'package:cubes_app/api/api_connection.dart' as api;

Future<http.Response> addContent(Content content) async {
  final response = await http.post(Uri.http(api.url, '/send_content'),
      headers: api.headers, body: contentToJson(content));
  return response;
}

Future<http.Response> addEventContent(Content content) async {
  final response = await http.post(Uri.http(api.url, '/send_content_event'),
      headers: api.headers, body: contentToJson(content));
  return response;
}