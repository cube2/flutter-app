import 'package:cubes_app/models/group_event.dart';
import 'package:http/http.dart' as http;
import 'package:cubes_app/api/api_connection.dart' as api;
import 'dart:convert';

Future<String> addEvent(GroupEvent event) async {
  final response = await http.post(Uri.http(api.url, '/create_event'),
      headers: api.headers, body: eventToJson(event));
  Map<String, dynamic> jsonBody = json.decode(response.body);
  return jsonBody["id_event"];
}

Future<GroupEvent> getEvent(String id) async {
  final response = await http.get(Uri.http(api.url, '/get_event/' + id),
      headers: api.headers);
  return eventFromJson(response.body);
}

Future<List<GroupEvent>> getFutureEvents() async {
  final response = await http.get(Uri.http(api.url, '/user_future_events'),
      headers: api.headers);
  Iterable parsedListJson = json.decode(response.body);
  return List<GroupEvent>.from(
      parsedListJson.map((model) => GroupEvent.fromJson(model)));
}
