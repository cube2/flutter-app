import 'dart:async';

import 'package:flutter/services.dart';
import 'package:pusher_websocket_flutter/pusher.dart';

class PusherService {
  Event lastEvent;
  String lastConnectionState;
  List<Channel> channels = <Channel>[];
  // StreamController<String> _eventData = StreamController<String>();
  StreamController<Event> _eventData = StreamController<Event>.broadcast();
  Sink get _inEventData => _eventData.sink;
  Stream get eventStream => _eventData.stream;
  final String _key = "6ece5d850174664dc087";
  // final String _key = "72146c9d372d58503d7d";
  final String _cluster = "eu";

  Future<void> firePusher(String channelName, String eventName) async {
    await initPusher();
    connectPusher();
    await subscribePusher(channelName);
    bindEvent(channelName, eventName);
  }

  Future<void> initPusher() async {
    try {
      await Pusher.init(_key, PusherOptions(cluster: _cluster));
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  void connectPusher() {
    Pusher.connect(onConnectionStateChange: (val) {
      print(val.currentState);
    }, onError: (err) {
      print(err.message);
    });
  }

  Future<void> subscribePusher(String channelName) async {
    if (channels.any((channel) => channel.name == channelName)) {
      return;
    }
    Channel channel = await Pusher.subscribe(channelName);
    channels.add(channel);
  }

  void unSubscribePusher(String channelName) {
    Pusher.unsubscribe(channelName);
    channels.removeWhere((channel) => channel.name == channelName);
  }

  void bindEvent(String channelName, String eventName) {
    Channel channel =
        channels.firstWhere((channel) => channel.name == channelName);
    channel.bind(eventName, (last) {
        // final String data = last.data;
        // _inEventData.add(data);
        _inEventData.add(last);
    });
  }

  void unbindEvent(String channelName, String eventName) {
    Channel channel =
        channels.firstWhere((channel) => channel.name == channelName);
    channel.unbind(eventName);
    _eventData.close();
  }

  void unSubscribeAll() {
    channels.forEach((channel) {
      String channelName = channel.name;
      Pusher.unsubscribe(channelName);
    });
    channels.clear();
    //channels.forEach((channel) => print(channel.name));
  }
}
