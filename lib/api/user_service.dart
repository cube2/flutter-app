import 'dart:convert';

import 'package:cubes_app/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:cubes_app/api/api_connection.dart' as api;

Future<User> getUser() async {
  final response = await http.get(Uri.http(api.url, '/1'));
  return userFromJson(response.body);
}

Future<List<User>> getActiveUsers() async {
  final response = await http.get(Uri.http(api.url, '/get_active_users'),
      headers: api.headers);
  Iterable parsedListJson = json.decode(response.body);
  return List<User>.from(parsedListJson.map((model) => User.fromJson(model)));
}

Future<List<User>> getActiveUsersButMe() async {
  List<User> users = await getActiveUsers();
  users.removeWhere((u) => u.id == api.id);
  return users;
}

Future<http.Response> addUser(User user) async {
  final response = await http.post(Uri.http(api.url, '/register'),
      headers: api.headers, body: userToJson(user));
  return response;
}
