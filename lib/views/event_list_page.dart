import 'package:cubes_app/api/event_service.dart';
import 'package:cubes_app/models/group_event.dart';
import 'package:cubes_app/widgets/ui/bottom_nav.dart';
import 'package:cubes_app/widgets/ui/event_detail.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';

class EventListPage extends StatefulWidget {
  EventListPage({Key key}) : super(key: key);

  @override
  _EventListPageState createState() => _EventListPageState();
}

class _EventListPageState extends State<EventListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            PaddingTop(value: 40),
            Text(
              "Évènements à venir",
              textScaleFactor: 1.7,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
            ),
            PaddingTop(),
            Expanded(
              child: FutureBuilder<List<GroupEvent>>(
                  future: getFutureEvents(),
                  builder:
                      (BuildContext context, AsyncSnapshot futureSnapshot) {
                    if (!futureSnapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    List<GroupEvent> events = futureSnapshot.data;
                    if (futureSnapshot.data.length == 0) {
                      return Center(
                        child: TextWithStyle(
                          "Aucun évènement disponible pour le moment",
                          color: Colors.grey[400],
                        ),
                      );
                    } else {
                      return ListView.builder(
                          itemCount: events.length,
                          itemBuilder: (context, i) {
                            GroupEvent event = events[i];
                            return Padding(
                              padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                              child: Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  EventDetail(event: event),
                                  Positioned(
                                      top: -5,
                                      right: -5,
                                      child: CircleAvatar(
                                        radius: 18,
                                        backgroundImage: event.groupPicture !=
                                                null
                                            ? NetworkImage(event.groupPicture)
                                            : null,
                                      )),
                                ],
                              ),
                            );
                          });
                    }
                  }),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNav(context),
    );
  }
}
