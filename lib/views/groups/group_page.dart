import 'package:cubes_app/api/group_service.dart';
import 'package:cubes_app/api/pusher_service.dart';
import 'package:cubes_app/api/user_service.dart';
import 'package:cubes_app/models/group.dart';
import 'package:cubes_app/models/user.dart';
import 'package:cubes_app/widgets/group_modal.dart';
import 'package:cubes_app/widgets/ui/bottom_nav.dart';
import 'package:cubes_app/widgets/ui/icon_bubble_button.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:cubes_app/widgets/user_groups_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:cubes_app/api/api_connection.dart' as api;

class GroupPage extends StatefulWidget {
  GroupPage({Key key}) : super(key: key);

  @override
  _GroupPageState createState() => _GroupPageState();
}

class _GroupPageState extends State<GroupPage> {
  List<Group> _groups = <Group>[];
  PusherService pusherService = PusherService();

  @override
  initState() {
    setPusherSubscriptions();
    super.initState();
  }

  Future<void> setPusherSubscriptions() async {
    //api call
    _groups = await getUserGroups();
    //pusher subscriptions
    await pusherService.initPusher();
    pusherService.connectPusher();
    _groups.forEach((group) async {
      await pusherService.subscribePusher('group_${group.id}');
      pusherService.bindEvent('group_${group.id}', 'receiveMessage');
    });
  }

  @override
  void dispose() {
    pusherService.unSubscribeAll();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  PaddingTop(value: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextWithStyle("Conversations",
                          factor: 1.7,
                          textStyle: TextStyle(fontWeight: FontWeight.w600)),
                      FutureBuilder<List<User>>(
                          future: getActiveUsers(),
                          builder: (BuildContext context,
                              AsyncSnapshot futureSnapshot) {
                            if (!futureSnapshot.hasData) {
                              return CircleAvatar(radius: 22.5);
                            }
                            List<User> _users = futureSnapshot.data;
                            User activeUser =
                                _users.firstWhere((user) => user.id == api.id);
                            return CircleAvatar(
                              radius: 22.5,
                              backgroundImage: activeUser.picture != null
                                  ? NetworkImage(activeUser.picture)
                                  : null,
                            );
                          }),
                    ],
                  ),
                  PaddingTop(),
                  // Align(
                  //   alignment: Alignment.centerLeft,
                  //   child: TextWithStyle(
                  //     "Messages privés",
                  //   ),
                  // ),
                  // PaddingTop(),
                  // Align(
                  //   alignment: Alignment.centerLeft,
                  //   child: IconBubbleButton(
                  //     onClick: () async {
                  //       //_activeUsers = await getActiveUsers();
                  //       print("clic");
                  //     },
                  //   ),
                  // ),
                  //PaddingTop(),
                ],
              )),
          Expanded(
            child: Container(
                padding: EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width * 0.7,
                decoration: BoxDecoration(
                    color: Color(0xff2C303E),
                    shape: BoxShape.rectangle,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(30.0))),
                child: Material(
                  color: Colors.transparent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          TextWithStyle("Mes groupes",
                              factor: 1.7,
                              textStyle:
                                  TextStyle(fontWeight: FontWeight.w600)),
                          Row(
                            children: [
                              IconButton(
                                icon: SvgPicture.asset(
                                  'assets/images/icn-search.svg',
                                  semanticsLabel: 'magnifying glass',
                                  width: 25,
                                ),
                                onPressed: () {},
                              ),
                              PaddingRight(value: 5),
                              Ink(
                                width: 36,
                                decoration: const ShapeDecoration(
                                  color: Color(0xFF6ECDB7),
                                  shape: CircleBorder(),
                                ),
                                child: IconButton(
                                  icon: SvgPicture.asset(
                                      'assets/images/icn-plus.svg',
                                      semanticsLabel: 'plus',
                                      width: 24),
                                  color: Colors.white,
                                  onPressed: () {
                                    groupModal();
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      Expanded(
                        child: UserGroupsList(
                          pusherService: pusherService,
                          groups: _groups,
                        ),
                      ),
                    ],
                  ),
                )),
          ),
        ],
      ),
      bottomNavigationBar: BottomNav(context),
    );
  }

  Future<void> groupModal() {
    return showMaterialModalBottomSheet(
        context: context,
        expand: false,
        barrierColor: Colors.black.withOpacity(0.7),
        builder: (context) {
          return GroupModal(pusherService: pusherService);
        });
  }
}
