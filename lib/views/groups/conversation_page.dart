import 'dart:async';

import 'package:cubes_app/api/content_service.dart';
import 'package:cubes_app/api/pusher_service.dart';
import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/models/group.dart';
import 'package:cubes_app/widgets/chat_message.dart';
import 'package:cubes_app/widgets/event_list_modal.dart';
import 'package:cubes_app/widgets/ui/chat_input.dart';
import 'package:cubes_app/widgets/ui/notification_circle.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pusher_websocket_flutter/pusher.dart';
import 'package:cubes_app/api/api_connection.dart' as api;

class ConversationPage extends StatefulWidget {
  final Group group;
  final PusherService pusherService;
  ConversationPage({Key key, @required this.group, this.pusherService})
      : super(key: key);

  @override
  _ConversationPageState createState() => _ConversationPageState();
}

class _ConversationPageState extends State<ConversationPage> {
  List<Content> contents = <Content>[];
  ScrollController scrollController = ScrollController();
  String _lastReceivedContentId;
  bool pusherNotFromParam;

  @override
  initState() {
    pusherNotFromParam = widget.pusherService.channels.isEmpty;
    if (pusherNotFromParam) {
      setPusherSubscription();
    }
    super.initState();
  }

  Future<void> setPusherSubscription() async {
    //pusher subscriptions
    await widget.pusherService.initPusher();
    widget.pusherService.connectPusher();
    await widget.pusherService.subscribePusher('group_${widget.group.id}');
    widget.pusherService
        .bindEvent('group_${widget.group.id}', 'receiveMessage');
  }

  @override
  void dispose() {
    if (pusherNotFromParam) {
      widget.pusherService.unSubscribeAll();
    }
    super.dispose();
  }

  Future<void> addContentToList(Content newContent) async {
    newContent.idGroup = widget.group.id;
    //call api to send message
    await addContent(newContent);
    setState(() {
      widget.group.contents.insert(0, newContent);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: SvgPicture.asset(
                'assets/images/icn-back.svg',
                semanticsLabel: 'Go back',
                width: 24,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: CircleAvatar(
              radius: 28,
              foregroundImage: widget.group.picture != null
                  ? NetworkImage(widget.group.picture)
                  : null,
            ),
            toolbarHeight: 80,
            centerTitle: true,
            actions: [
              IconButton(
                icon: widget.group.events.isNotEmpty
                    ? Stack(
                        clipBehavior: Clip.none,
                        children: [
                          SvgPicture.asset(
                            'assets/images/icn-calendar.svg',
                            semanticsLabel: 'calendar',
                            width: 24,
                          ),
                          Positioned(
                            top: -10,
                            right: -10,
                            child:
                                NotificationCircle(widget.group.events.length),
                          ),
                        ],
                      )
                    : SvgPicture.asset(
                        'assets/images/icn-calendar.svg',
                        semanticsLabel: 'calendar',
                        width: 24,
                      ),
                onPressed: showEventListModal,
              ),
              PaddingRight(value: 10),
            ],
            backgroundColor: Color(0xff2C303E),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: StreamBuilder(
                    stream: widget.pusherService.eventStream,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        Event pusher = snapshot.data;
                        if (pusher.event == "receiveMessage" &&
                            pusher.channel == 'group_${widget.group.id}') {
                          Content newContent = contentFromJson(pusher.data);
                          if (_lastReceivedContentId != newContent.id &&
                              newContent.author.id != api.id) {
                            widget.group.contents.insert(0, newContent);
                            _lastReceivedContentId = newContent.id;
                          }
                        }
                      }
                      return ListView.builder(
                          controller: scrollController,
                          reverse: true,
                          itemCount: widget.group.contents.length,
                          itemBuilder: (context, i) {
                            return ChatMessage(
                                content: widget.group.contents[i]);
                          });
                    }),
              ),
              PaddingTop(value: 12),
              ChatInput(
                  notifyParent: addContentToList, groupId: widget.group.id, isGroup: true,)
            ],
          ),
        ),
      ],
    );
  }

  Future<void> showEventListModal() {
    return showMaterialModalBottomSheet(
        context: context,
        expand: false,
        barrierColor: Colors.black.withOpacity(0.3),
        builder: (context) {
          return EventListModal(events: widget.group.events);
        });
  }
}
