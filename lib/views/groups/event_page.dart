import 'package:cubes_app/api/content_service.dart';
import 'package:cubes_app/api/pusher_service.dart';
import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/models/group_event.dart';
import 'package:cubes_app/widgets/chat_message.dart';
import 'package:cubes_app/widgets/ui/chat_input.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:pusher_websocket_flutter/pusher.dart';
import 'package:cubes_app/api/api_connection.dart' as api;

class EventPage extends StatefulWidget {
  final GroupEvent event;
  // final PusherService pusherService;
  EventPage({Key key, @required this.event}) : super(key: key);

  @override
  _EventPageState createState() => _EventPageState();
}

class _EventPageState extends State<EventPage> {
  PusherService pusherService = PusherService();
  List<Content> contents = <Content>[];
  ScrollController scrollController = ScrollController();
  String _lastReceivedContentId;

  @override
  initState() {
    setPusherSubscription();
    super.initState();
  }

  Future<void> setPusherSubscription() async {
    await pusherService.subscribePusher('event_${widget.event.id}');
    pusherService.bindEvent('event_${widget.event.id}', 'receiveMessage');
  }

  @override
  void dispose() {
    pusherService.unbindEvent('event_${widget.event.id}', 'receiveMessage');
    pusherService.unSubscribePusher('event_${widget.event.id}');
    super.dispose();
  }

  Future<void> addContentToList(Content newContent) async {
    newContent.idEvent = widget.event.id;
    //call api to send message
    await addEventContent(newContent);
    setState(() {
      widget.event.contents.insert(0, newContent);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: SvgPicture.asset(
                'assets/images/icn-back.svg',
                semanticsLabel: 'Go back',
                width: 24,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text(widget.event.name),
            toolbarHeight: 80,
            centerTitle: true,
            backgroundColor: Color(0xff2C303E),
            actions: [
              CircleAvatar(
                radius: 18,
                foregroundImage: widget.event.picture != null
                    ? NetworkImage(widget.event.picture)
                    : null,
              ),
              PaddingRight(value: 10)
            ],
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: StreamBuilder(
                    stream: pusherService.eventStream,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        Event pusher = snapshot.data;
                        if (pusher.event == "receiveMessage" &&
                            pusher.channel == 'event_${widget.event.id}') {
                          Content newContent = contentFromJson(pusher.data);
                          if (_lastReceivedContentId != newContent.id &&
                              newContent.author.id != api.id) {
                            widget.event.contents.insert(0, newContent);
                            _lastReceivedContentId = newContent.id;
                          }
                        }
                      }
                      return ListView.builder(
                          controller: scrollController,
                          reverse: true,
                          itemCount: widget.event.contents.length,
                          itemBuilder: (context, i) {
                            return ChatMessage(
                                content: widget.event.contents[i]);
                          });
                    }),
              ),
              PaddingTop(value: 12),
              ChatInput(notifyParent: addContentToList, isGroup: false)
            ],
          ),
        ),
      ],
    );
  }
}
