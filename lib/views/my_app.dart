import 'package:cubes_app/models/theme.dart';
import 'package:cubes_app/routes.dart';
import 'package:flutter/material.dart';
import 'package:cubes_app/views/home_page.dart';
import 'package:cubes_app/views/auth/index_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class MyApp extends StatelessWidget {
  final bool isConnected = true;

  @override
  Widget build(BuildContext context) {
    //dismiss keyboard properly
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus &&
            currentFocus.focusedChild != null) {
          currentFocus.focusedChild.unfocus();
        }
      },
      child: MaterialApp(
        title: 'Flutter Agora app',
        debugShowCheckedModeBanner: false,
        theme: MyTheme.darkTheme,
        home: (isConnected) ? HomePage() : IndexPage(title: 'Agora App'),
        routes: routes,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        supportedLocales: [Locale('fr', 'FR')],
      ),
    );
  }
}
