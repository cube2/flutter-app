import 'package:cubes_app/api/group_service.dart';
import 'package:cubes_app/api/pusher_service.dart';
import 'package:cubes_app/models/group.dart';
import 'package:cubes_app/widgets/public_group_modal.dart';
import 'package:cubes_app/widgets/ui/bottom_nav.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PusherService pusherService = new PusherService();
  @override
  void initState() {
    pusherService.unSubscribeAll();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PaddingTop(
                value: 60,
              ),
              Row(
                children: [
                  PaddingRight(value: 10),
                  TextWithStyle("Les plus populaires",
                      factor: 1.7,
                      textStyle: TextStyle(fontWeight: FontWeight.w600)),
                ],
              ),
              PaddingTop(),
              Container(
                height: 336,
                child: FutureBuilder<List<Group>>(
                  future: getPublicGroups(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (!snapshot.hasData) {
                      return Center(child: CircularProgressIndicator());
                    }
                    return ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, i) {
                          Group group = snapshot.data[i];
                          return preparePopularListItem(group);
                        });
                  },
                ),
              ),
              PaddingTop(),
              Row(
                children: [
                  PaddingRight(value: 10),
                  TextWithStyle("Groupes publics",
                      factor: 1.7,
                      textStyle: TextStyle(fontWeight: FontWeight.w600)),
                ],
              ),
              PaddingTop(value: 10),
              Container(
                // Expanded(
                child: FutureBuilder<List<Group>>(
                    future: getPublicGroups(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (!snapshot.hasData) {
                        return Center(child: CircularProgressIndicator());
                      }
                      return ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: snapshot.data.length,
                          itemBuilder: (context, i) {
                            Group group = snapshot.data[i];
                            return prepareListItem(group);
                          });
                    }),
              )
            ],
          ),
        ),
        bottomNavigationBar: BottomNav(context));
  }

  Widget preparePopularListItem(Group group) {
    double circleRadius = 40;
    double insideBorderRadius = 5;
    double containerHeight = 278;
    return Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 14),
        child: InkWell(
          onTap: () => showGroupModal(group),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(top: circleRadius),
                child: Container(
                    padding: EdgeInsets.fromLTRB(15, circleRadius + 10, 15, 10),
                    width: MediaQuery.of(context).size.width * 0.7,
                    height: containerHeight,
                    decoration: BoxDecoration(
                        color: Color(0xff2C303E),
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextWithStyle(
                          group.name,
                          factor: 1.4,
                          textStyle: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        PaddingTop(),
                        Text(group.description,
                            textScaleFactor: 0.9,
                            style: TextStyle(
                                color: Colors.white.withOpacity(0.5),
                                fontWeight: FontWeight.w600)),
                      ],
                    )),
              ),
              Positioned(
                left: 20,
                top: 0,
                child: CircleAvatar(
                  radius: circleRadius,
                  backgroundColor: Color(0xFF171822),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(group.picture),
                    radius: circleRadius - insideBorderRadius,
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Widget prepareListItem(Group group) {
    double circleRadius = 40;
    double insideBorderRadius = 5;
    double containerHeight = 118;
    return Padding(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 14),
        child: InkWell(
          onTap: () => showGroupModal(group),
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(left: circleRadius),
                child: Container(
                    padding: EdgeInsets.fromLTRB(circleRadius + 20, 20, 20, 20),
                    width: MediaQuery.of(context).size.width - circleRadius,
                    height: containerHeight,
                    decoration: BoxDecoration(
                        color: Color(0xff2C303E),
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(20.0))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextWithStyle(group.name,
                            factor: 1.2,
                            textStyle: TextStyle(fontWeight: FontWeight.w600)),
                        PaddingTop(),
                        Flexible(
                          child: Text(group.description,
                              textScaleFactor: 0.8,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 3,
                              style: TextStyle(
                                  color: Colors.white.withOpacity(0.5),
                                  fontWeight: FontWeight.w600)),
                        )
                      ],
                    )),
              ),
              Positioned(
                left: 0,
                top: (containerHeight - circleRadius * 2) / 2,
                child: CircleAvatar(
                  radius: circleRadius,
                  backgroundColor: Color(0xFF171822),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(group.picture),
                    radius: circleRadius - insideBorderRadius,
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Future<void> showGroupModal(Group group) async {
    group = await getGroup(group.id);
    return showMaterialModalBottomSheet(
        context: context,
        expand: false,
        barrierColor: Colors.black.withOpacity(0.3),
        builder: (context) {
          return PublicGroupModal(group: group);
        });
  }
}
