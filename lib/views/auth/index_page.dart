import 'package:cubes_app/widgets/ui/custom_elevated_button.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:flutter/material.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter_svg/svg.dart';

class IndexPage extends StatefulWidget {
  IndexPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width * 0.8;

    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: SizedBox(
            width: _width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/images/agora_logo.png'),
                PaddingTop(),
                Text(
                  "Agora",
                  textScaleFactor: 4,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                PaddingTop(),
                TextWithStyle(
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum he industry's standard ,",
                  factor: 0.8,
                  color: Colors.grey,
                ),
                PaddingTop(),
                CustomElevatedButton(
                  "Inscription",
                  width: _width,
                  onPressed: () => Navigator.pushNamed(context, "/register"),
                ),
                PaddingTop(),
                CustomElevatedButton(
                  "Connexion",
                  width: _width,
                  onPressed: () => Navigator.pushNamed(context, "/login"),
                  primary: Colors.grey[800],
                  textColor: Colors.white,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
