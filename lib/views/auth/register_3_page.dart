import 'package:cubes_app/api/user_service.dart';
import 'package:cubes_app/models/user.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/rounded_outlined_button.dart';
import 'package:cubes_app/widgets/ui/rounded_text_input.dart';
import 'package:flutter/material.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';

class Register3Page extends StatefulWidget {
  Register3Page({Key key}) : super(key: key);

  @override
  _Register3PageState createState() => _Register3PageState();
}

class _Register3PageState extends State<Register3Page> {
  int phonePrefix;
  String phone;

  @override
  Widget build(BuildContext context) {
    final User user = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(),
        body: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextWithStyle(
                  "Bonjour,",
                  factor: 1.3,
                ),
                TextWithStyle(user.firstname),
                PaddingTop(value: 60),
                RoundedTextInput(
                  keyboardType: TextInputType.phone,
                  onChanged: (String s) {
                    setState(() {
                      user.phone = s;
                    });
                  },
                  label: "Numéro de téléphone",
                ),
                PaddingTop(value: 60),
                RoundedOutlinedButton("Suivant", onPressed: () {
                  addUser(user).then((response) => print(response.body));
                  Navigator.pushNamedAndRemoveUntil(
                      context, "/home", (route) => false);
                }),
              ],
            ),
          ),
        ));
  }
}
