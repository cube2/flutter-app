import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/rounded_outlined_button.dart';
import 'package:cubes_app/widgets/ui/rounded_text_input.dart';
import 'package:flutter/material.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:flutter_svg/svg.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email;
  String password;

  @override
  Widget build(BuildContext context) {
    final double _width = MediaQuery.of(context).size.width * 0.8;

    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: SvgPicture.asset(
              'assets/images/icn-back.svg',
              semanticsLabel: 'Go back',
              width: 24,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: Color(0xFF171822),
        ),
        body: Center(
          child: SizedBox(
            width: _width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextWithStyle(
                  "Connexion",
                  factor: 1.3,
                ),
                TextWithStyle("Lorem ipsum sic amet"),
                PaddingTop(value: 40),
                RoundedTextInput(
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (String s) {
                    email = s;
                  },
                  label: "Adresse email",
                ),
                PaddingTop(),
                RoundedTextInput(
                  keyboardType: TextInputType.visiblePassword,
                  isPassword: true,
                  onChanged: (String s) {
                    password = s;
                  },
                  label: "Mot de passe",
                ),
                PaddingTop(value: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RoundedOutlinedButton(
                      "Connexion",
                      onPressed: () {
                        if (email != null && password != null) {
                          Navigator.pushNamedAndRemoveUntil(
                              context, "/home", (route) => false);
                        }
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
