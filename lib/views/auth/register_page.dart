import 'package:cubes_app/views/auth/register_2_page.dart';
import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/padding_right.dart';
import 'package:cubes_app/widgets/ui/rounded_outlined_button.dart';
import 'package:cubes_app/widgets/ui/rounded_text_input.dart';
import 'package:flutter/material.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:cubes_app/models/user.dart';
import 'package:flutter_svg/svg.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  User user = new User();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: SvgPicture.asset(
              'assets/images/icn-back.svg',
              semanticsLabel: 'Go back',
              width: 24,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: Color(0xFF171822),
        ),
        body: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextWithStyle(
                  "Bonjour,",
                  factor: 2.2,
                ),
                TextWithStyle("Présente-toi !"),
                PaddingTop(),
                RoundedTextInput(
                  onChanged: (String s) {
                    setState(() {
                      user.firstname = s;
                    });
                  },
                  label: "Prénom",
                ),
                PaddingTop(),
                RoundedTextInput(
                  onChanged: (String s) {
                    setState(() {
                      user.lastname = s;
                    });
                  },
                  label: "Nom",
                ),
                PaddingTop(),
                RoundedOutlinedButton(
                  "Suivant",
                  onPressed: () {
                    if (user.firstname != null && user.lastname != null) {
                      Navigator.pushNamed(context, "/register2",
                          arguments: user);
                    }
                  },
                ),
                PaddingTop(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.white,
                        shape: CircleBorder(),
                      ),
                      child: IconButton(
                        icon: Icon(Icons.android),
                        color: Colors.black,
                        onPressed: () {},
                      ),
                    ),
                    PaddingRight(),
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.white,
                        shape: CircleBorder(),
                      ),
                      child: IconButton(
                        icon: Icon(Icons.android),
                        color: Colors.black,
                        onPressed: () {},
                      ),
                    ),
                    PaddingRight(),
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.white,
                        shape: CircleBorder(),
                      ),
                      child: IconButton(
                        icon: Icon(Icons.android),
                        color: Colors.black,
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ));
  }
}
