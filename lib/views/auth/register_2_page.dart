import 'package:cubes_app/widgets/ui/padding_top.dart';
import 'package:cubes_app/widgets/ui/rounded_outlined_button.dart';
import 'package:cubes_app/widgets/ui/rounded_text_input.dart';
import 'package:flutter/material.dart';
import 'package:cubes_app/widgets/ui/text_with_style.dart';
import 'package:cubes_app/models/user.dart';
import 'package:flutter_svg/svg.dart';

class Register2Page extends StatefulWidget {
  Register2Page({Key key}) : super(key: key);

  @override
  _Register2PageState createState() => _Register2PageState();
}

class _Register2PageState extends State<Register2Page> {
  String passwordConfirmation;

  @override
  Widget build(BuildContext context) {
    final User user = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: SvgPicture.asset(
              'assets/images/icn-back.svg',
              semanticsLabel: 'Go back',
              width: 24,
            ),
            onPressed: () => Navigator.of(context).pop(),
          ),
          backgroundColor: Color(0xFF171822),
        ),
        body: Center(
          child: SingleChildScrollView(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                TextWithStyle(
                  "Bonjour,",
                  factor: 1.3,
                ),
                TextWithStyle(user.firstname),
                PaddingTop(value: 40),
                RoundedTextInput(
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (String s) {
                    user.email = s;
                  },
                  label: "Adresse email",
                ),
                PaddingTop(),
                RoundedTextInput(
                  keyboardType: TextInputType.visiblePassword,
                  isPassword: true,
                  onChanged: (String s) {
                    user.password = s;
                  },
                  label: "Créer un mot de passe",
                ),
                PaddingTop(),
                RoundedTextInput(
                  keyboardType: TextInputType.visiblePassword,
                  isPassword: true,
                  onChanged: (String s) {
                    passwordConfirmation = s;
                  },
                  label: "Répéter votre mot de passe",
                ),
                PaddingTop(value: 40),
                RoundedOutlinedButton(
                  "Suivant",
                  onPressed: () {
                    if (user.email != null &&
                        user.password != null &&
                        (passwordConfirmation == user.password)) {
                      Navigator.pushNamed(context, "/register3", arguments: user);
                    }
                  },
                ),
              ],
            ),
          ),
        ));
  }
}
