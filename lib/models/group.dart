// To parse this JSON data, do
//
//     final group = groupFromJson(jsonString);

import 'dart:convert';
import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/models/group_event.dart';
import 'package:cubes_app/models/member.dart';

Group groupFromJson(String str) => Group.fromJson(json.decode(str));

String groupToJson(Group data) => json.encode(data.toJson());

class Group {
  Group(
      {this.id,
      this.name,
      this.description,
      this.picture,
      this.isPublic,
      this.theme,
      this.createdAt,
      this.members,
      this.contents,
      this.events,
      this.unreadContents});

  String id;
  String name;
  String description;
  String picture;
  bool isPublic;
  String theme;
  DateTime createdAt;
  List<Member> members;
  List<Content> contents;
  List<GroupEvent> events;
  int unreadContents;
  List<String> userIds;

  factory Group.fromJson(Map<String, dynamic> json) => Group(
        id: json["id"],
        name: json["name"],
        description: json["description"],
        picture: json["picture"],
        isPublic: json["isPublic"] ?? false,
        theme: json["theme"],
        createdAt: DateTime.parse(
            json["createdAt"] ?? DateTime.now().toIso8601String()),
        members: List<Member>.from(
            json["members"]?.map((x) => Member.fromJson(x)) ?? []),
        contents: List<Content>.from(
            json["contents"]?.map((x) => Content.fromJson(x)) ?? []),
        events: List<GroupEvent>.from(
            json["events"]?.map((x) => GroupEvent.fromJson(x)) ?? []),
        unreadContents: json["unreadContents"] ?? 0,
        // unreadContents: (json["unread_contents"] != null) ? json["unread_contents"] : 0, //same as previous row (null operator)
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "picture": picture,
        "isPublic": isPublic,
        "theme": theme,
        "user_ids": userIds
        //"createdAt": createdAt.toIso8601String(),
        //"members": List<dynamic>.from(members.map((x) => x.toJson())),
        //"contents": List<dynamic>.from(contents.map((x) => x.toJson())),
      };
}
