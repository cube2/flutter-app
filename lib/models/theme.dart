import 'package:flutter/material.dart';

const PrimaryColor = const Color(0xFF008080);
const PrimaryColorLight = const Color(0xFF4cb0af);
const PrimaryColorDark = const Color(0xFF005354);

const SecondaryColor = const Color(0xFF6ECDB7);
const SecondaryColorLight = const Color(0xFFe5ffff);
const SecondaryColorDark = const Color(0xFF82ada9);

const Background = const Color(0xFFfffdf7);

const TextColor = const Color(0xFF004d40);
const MainFont = "Montserrat";

class MyTheme {
  static final ThemeData lightTheme = _buildLightTheme();
  static final ThemeData darkTheme = _buildDarkTheme();

  static ThemeData _buildLightTheme() {
    final ThemeData base = ThemeData.light();

    return base.copyWith(      
      canvasColor: Colors.transparent,
      accentColor: SecondaryColor,
      accentColorBrightness: Brightness.dark,
      primaryColor: PrimaryColor,
      primaryColorDark: PrimaryColorDark,
      primaryColorLight: PrimaryColorLight,
      primaryColorBrightness: Brightness.dark,
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: SecondaryColor,
        textTheme: ButtonTextTheme.primary,
      ),
      // scaffoldBackgroundColor: DarkBackground,
      cardColor: Background,
      textSelectionColor: PrimaryColorLight,
      // backgroundColor: DarkBackground,
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        foregroundColor: SecondaryColor,
      ),
      textTheme: base.textTheme
          .copyWith(
              title: base.textTheme.title.copyWith(color: TextColor),
              body1: base.textTheme.body1.copyWith(color: TextColor),
              body2: base.textTheme.body2.copyWith(color: TextColor))
          .apply(fontFamily: MainFont),
    );
  }

  static ThemeData _buildDarkTheme() {
    final ThemeData base = ThemeData.dark();

    const DarkBackground = const Color(0xFF171822);
    const DarkTextColor = const Color(0xFFFFFFFF);

    return base.copyWith(      
      canvasColor: Colors.transparent,
      accentColor: SecondaryColor,
      accentColorBrightness: Brightness.dark,
      primaryColor: PrimaryColor,
      primaryColorDark: PrimaryColorDark,
      primaryColorLight: PrimaryColorLight,
      primaryColorBrightness: Brightness.dark,
      buttonTheme: base.buttonTheme.copyWith(
        buttonColor: SecondaryColor,
        textTheme: ButtonTextTheme.primary,
      ),
      scaffoldBackgroundColor: DarkBackground,
      cardColor: DarkBackground,
      colorScheme: ColorScheme.dark(primary: SecondaryColor),
      textSelectionColor: PrimaryColorLight,
      backgroundColor: DarkBackground,
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        foregroundColor: SecondaryColor,
      ),
      textTheme: base.textTheme
          .copyWith(
              title: base.textTheme.title.copyWith(color: DarkTextColor),
              body1: base.textTheme.body1.copyWith(color: DarkTextColor),
              body2: base.textTheme.body2.copyWith(color: DarkTextColor))
          .apply(fontFamily: MainFont),
    );
  }
}
