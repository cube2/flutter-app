// To parse this JSON data, do
//
//     final content = contentFromJson(jsonString);

import 'dart:convert';
import 'package:cubes_app/models/user.dart';
import 'package:intl/intl.dart';

Content contentFromJson(String str) => Content.fromJson(json.decode(str));

String contentToJson(Content data) => json.encode(data.toJson());

class Content {
  Content({
    this.id,
    this.value,
    this.author,
    this.type,
    this.createdAt,
    this.idGroup,
    this.idEvent,
  });

  String id;
  String value;
  User author;
  String type;
  DateTime createdAt;
  String idGroup;
  String idEvent;

  //return time for today and date for the other days
  String getDateOrTime() {
    final now = DateTime.now();
    final today = DateTime(now.year, now.month, now.day);
    final aDate = DateTime(createdAt.year, createdAt.month, createdAt.day);
    return aDate == today
        ? DateFormat('Hm').format(createdAt)
        : DateFormat('dd/MM/yyyy').format(createdAt);
  }

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        id: json["id"],
        value: json["value"],
        author: User.fromJson(json["author"]),
        type: json["type"],
        createdAt: DateTime.parse(json["createdAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "value": value,
        "author": author,
        "type": type,
        //"created_at": createdAt?.toJson(),
        "id_group": idGroup,
        "id_event": idEvent,
      };
}
