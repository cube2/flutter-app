class Member {
  Member({
    this.userId,
    this.role,
    this.isBanned,
    this.pendingInvitation,
  });

  String userId;
  String role;
  bool isBanned;
  bool pendingInvitation;

  factory Member.fromJson(Map<String, dynamic> json) => Member(
        userId: json["userId"],
        role: json["role"],
        isBanned: json["is_banned"],
        pendingInvitation: json["pending_invitation"],
      );

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "role": role,
        "is_banned": isBanned,
        "pending_invitation": pendingInvitation,
      };
}
