import 'dart:convert';

import 'package:cubes_app/models/content.dart';
import 'package:cubes_app/models/user.dart';

GroupEvent eventFromJson(String str) => GroupEvent.fromJson(json.decode(str));

String eventToJson(GroupEvent data) => json.encode(data.toJson());

class GroupEvent {
  GroupEvent({
    this.id,
    this.groupId,
    this.name,
    this.description,
    this.picture,
    this.groupPicture,
    this.date,
    this.location,
    this.createdAt,
    this.contents,
    this.author,
  });

  String id;
  String groupId;
  String name;
  String description;
  String picture;
  String groupPicture;
  DateTime date;
  String location;
  DateTime createdAt;
  List<Content> contents;
  User author;

  factory GroupEvent.fromJson(Map<String, dynamic> json) => GroupEvent(
        id: json["id"],
        groupId: json["id_group"],
        name: json["name"],
        description: json["description"],
        picture: json["picture"],
        groupPicture: json["group_picture"],
        location: json["location"],
        date: DateTime.parse(
            json["date_event"] ?? DateTime.now().toIso8601String()),
        createdAt: DateTime.parse(
            json["createdAt"] ?? DateTime.now().toIso8601String()),
        author: User.fromJson(json["author"]),
        contents: List<Content>.from(
            json["contents"]?.map((x) => Content.fromJson(x)) ?? []),
      );

  Map<String, dynamic> toJson() => {
        "id_group": groupId,
        "name": name,
        "description": description,
        "picture": picture,
        "location": location,
        "date_event": date.toIso8601String(),
      };
}
