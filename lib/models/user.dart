// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User user) => json.encode(user.toJson());

class User {
    User({
        this.id,
        this.firstname,
        this.lastname,
        this.email,
        this.password,
        this.picture,
        this.createdAt,
        this.phone,
        this.isActive,
        this.role,
    });

    String id;
    String firstname;
    String lastname;
    String email;
    String password;
    String picture;
    DateTime createdAt;
    dynamic phone;
    bool isActive;
    String role;

    String getFullName() => firstname + " " + lastname;
    

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        firstname: json["firstname"],
        lastname: json["lastname"],
        email: json["email"],
        password: json["password"],
        picture: json["picture"],
        //createdAt: CreatedAt.fromJson(json["CreatedAt"]),
        phone: json["phone"],
        isActive: json["isActive"],
        role: json["role"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "firstname": firstname,
        "lastname": lastname,
        "email": email,
        "password": password,
        "picture": picture,
        "phone": phone,
        "isActive": isActive,
        "role": role,
    };
}