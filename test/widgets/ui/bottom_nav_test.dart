import 'package:cubes_app/views/groups/group_page.dart';
import 'package:cubes_app/views/home_page.dart';
import 'package:cubes_app/views/my_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  group('Navigation tests', () {
    NavigatorObserver mockObserver;

    setUp(() {
      mockObserver = MockNavigatorObserver();
    });

    //Future<void> _buildHomePage(WidgetTester tester) async {
    //await tester.pumpWidget(MaterialApp(
    //home: HomePage(),

    // This mocked observer will now receive all navigation events
    // that happen in our app.
    //navigatorObservers: [mockObserver],
    //));

    // The tester.pumpWidget() call above just built our app widget
    // and triggered the pushObserver method on the mockObserver once.
    //verify(mockObserver.didPush(any, any));
    //}

    Future<void> _buildHomePage(WidgetTester tester) async {
      await tester.pumpWidget(MyApp());
    }

    Future<void> _navigateToGroupPage(WidgetTester tester) async {
      // Tap the button which should navigate to the details page.
      //
      // By calling tester.pumpAndSettle(), we ensure that all animations
      // have completed before we continue further.
      await tester.tap(find.text('Conversations'));
      await tester.pumpAndSettle();
    }

    testWidgets('when opening the app, should arrive at home page',
        (WidgetTester tester) async {
      // create the widget
      await tester.pumpWidget(MyApp());

      // create the finders
      final titleFinder = find.text('Les plus populaires');

      expect(titleFinder, findsOneWidget);
    });

    // testWidgets(
    //     'when tapping "Conversations" button, should navigate to group page',
    //     (WidgetTester tester) async {
    //   await _buildHomePage(tester);
    //   await _navigateToGroupPage(tester);

    //   // verify(mockObserver.didPush(any, any));

    //   expect(find.byType(GroupPage), findsOneWidget);

    //   expect(find.text('Mes groupes'), findsOneWidget);
    // });
  });
}
